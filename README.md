# elizaolkinitskaya.com

## local development

```
npm install
npm start
```

## git-auto-deploy

Deploying (on every git push) to elizaolkinitskaya.com (on digitalocean) via git-auto-deploy script:

https://github.com/olipo186/Git-Auto-Deploy

Config file for git-auto-deploy script on digitalocean server (use the montrealist e-mail as login to see the droplet): 

cat /etc/git-auto-deploy.conf.json
