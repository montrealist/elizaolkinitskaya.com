var gulp = require('gulp');
var gls = require('gulp-live-server');

var port = 3000;
gulp.task('serve', function() {
  console.log(`serving at port ${port}`);
  //2. serve at custom port
  var server = gls.static('public', port);
  server.start();

  //use gulp.watch to trigger server actions(notify, start or stop)
  gulp.watch(['public/assets/**/*.css', 'public/**/*.html'], function (file) {
    server.notify.apply(server, [file]);
  });
});